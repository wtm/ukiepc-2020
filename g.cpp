#include <bits/stdc++.h>

using namespace std;

int main() {
  int nb_cities, nb_plant_loc;
  cin >> nb_cities >> nb_plant_loc;
  vector<int> plant_loc(nb_plant_loc);
  vector<int> cost_of_plant(nb_plant_loc);
  for (int i = 0; i < nb_plant_loc; i ++) {
    cin >> plant_loc[i] >> cost_of_plant[i];
  }
  vector<int> cost_of_line(nb_cities);
  for (int& c : cost_of_line) {
    cin >> c;
  }
  return 0;
}
