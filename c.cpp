#include <bits/stdc++.h>

using namespace std;

int main() {
  int n, p;
  cin >> n >> p;
  vector<int> ts(n);
  for (int &t : ts) {
    cin >> t;
  }
  int curr = 0;
  vector<int> ans(n);
  if (ts.back() != 0) {
    curr = 1;
  }
  ans[n - 1] = curr;
  for (ssize_t i = ts.size() - 2; i >= 0; i --) {
    int t = ts[i];
    int last_t = ts[i + 1];
    if (t > last_t) {
      curr ++;
    }
    ans[i] = curr;
  }
  if (curr != p && curr != 0) {
    cout << "ambiguous" << endl;
  } else {
    for (int a : ans) {
      cout << a << '\n';
    }
  }
  cout.flush();
  return 0;
}
