#include <bits/stdc++.h>

using namespace std;

int main() {
  int n, k;
  cin >> n >> k;
  vector<int> current_pos(k);
  for (int& cp : current_pos) {
    cin >> cp;
  }
  vector<int> desired_pos(k);
  for (int& d : desired_pos) {
    cin >> d;
  }

  return 0;
}
