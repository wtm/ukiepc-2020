#include <bits/stdc++.h>

using namespace std;

optional<pair<int, int>> solve(int c, int e, int m) {
  const optional<pair<int, int>> f;
  if (c != 4) {
    return f;
  }
  // e = 2((w - 2) + (h - 2));
  if (e % 2 != 0) {
    return f;
  }
  // w + h
  auto a = (double) e / 2.0 + 4.0;
  // m = (w - 2)*(h - 2)
  double sq_term = sqrt(a * a - 8.0 * a - 4.0 * ((double) m) + 16.0);
  if (isnan(sq_term)) {
    return f;
  }
  double w = ((double) a + sq_term) / 2.0;
  if (w < 0) {
    w = ((double) a - sq_term) / 2.0;
  }
  double h = a - w;
  if (w <= 0 || h <= 0 || abs(round(w) - w) > 0.0001 || abs(round(h) - h) > 0.0001) {
    return f;
  }
  if (w < h) {
    swap(w, h);
  }
  int iw = (int) round(w);
  int ih = (int) round(h);
  if ((iw - 2)*(ih - 2) != m || 2*(iw - 2 + ih - 2) != e) {
    // ???
    return f;
  }
  return make_optional<pair<int, int>>(iw, ih);
}

int main() {
  int c, e, m;
  cin >> c >> e >> m;
  auto res = solve(c, e, m);
  if (res.has_value()) {
    cout << res->first << ' ' << res->second << endl;
  } else {
    cout << "impossible" << endl;
  }
}
