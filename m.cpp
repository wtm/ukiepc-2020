#include <bits/stdc++.h>

using namespace std;

int main() {
  int n;
  cin >> n;
  vector<int> a(n), b(n), c(n);
  for (int& i : a) {
    cin >> i;
  }
  for (int& i : b) {
    cin >> i;
  }
  for (int& i : c) {
    cin >> i;
  }
  for (int i = 0; i < n; i ++) {
    if (i != 0) {
      cout << ' ';
    }
    array<int, 3> nbs{a[i], b[i], c[i]};
    sort(nbs.begin(), nbs.end());
    cout << nbs[1];
  }
  cout << endl;
}
