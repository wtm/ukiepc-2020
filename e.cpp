#include <bits/stdc++.h>

using namespace std;

int main() {
  int h, w;
  cin >> h >> w;
  vector<vector<int>> hmap(h, vector<int>(w));
  const auto comp = [&](const pair<int, int>& a, const pair<int, int>& b) -> bool {
    int ah = hmap[a.second][a.first];
    int bh = hmap[b.second][b.first];
    return ah < bh;
  };
//  priority_queue<pair<int, int>, vector<pair<int, int>>, decltype(comp)> pq(comp);
  vector<pair<int, int>> queue;
  for (int y = 0; y < h; y ++) {
    auto& line = hmap[y];
    for (int x = 0; x < w; x ++) {
      cin >> line[x];
      queue.emplace_back(x, y);
    }
  }
  sort(queue.begin(), queue.end(), comp);
  vector<vector<bool>> done(h, vector<bool>(w));
  int ans = 0;
  for (ssize_t i = queue.size() - 1; i >= 0; i --) {
    auto pos = queue[i];
    int thish = hmap[pos.second][pos.first];
    if (thish <= 1) {
      break;
    }
    function<void(int, int, int)> dfs = [&](int last_h, int x, int y) {
      if (x < 0 || x >= w || y < 0 || y >= h) {
        return;
      }
      int curr_h = hmap[y][x];
      if (done[y][x] || curr_h > last_h) {
        return;
      }
      done[y][x] = true;
      dfs(curr_h, x - 1, y);
      dfs(curr_h, x + 1, y);
      dfs(curr_h, x, y - 1);
      dfs(curr_h, x, y + 1);
    };
    if (done[pos.second][pos.first]) {
      continue;
    }
    ans ++;
    dfs(thish, pos.first, pos.second);
  }
  cout << ans << endl;
  return 0;
}
